import React, { useRef, Suspense } from 'react'

import useCurrentUrl from 'shared/hooks/use-current-url'
import { Provider as HistoryProvider } from 'shared/hooks/use-history'
import useRoute from 'shared/hooks/use-route'

export default function Router ({ history, ...props }) {
  let url = useCurrentUrl(history)
  let { Component, matches } = useRoute(url)
  if (!Component) {
    return null
  }

  return (
    <HistoryProvider value={history}>
      <Suspense fallback={<div>suspended loading...</div>}>
        <Component {...props} {...matches} />
      </Suspense>
    </HistoryProvider>
  )
}
