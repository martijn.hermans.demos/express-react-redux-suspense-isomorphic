import React from 'react'
import ReactDOM from 'react-dom'
import Router from './router'
import { createBrowserHistory } from 'history'
import createStore from 'shared/store'
import { Provider as ReduxProvider } from 'react-redux'

const history = createBrowserHistory()

const store = createStore(window.__PRELOADED_STATE__)

ReactDOM.hydrate(
  <ReduxProvider store={store}>
    <Router history={history} />
  </ReduxProvider>,
  document.querySelector('#react-app')
)
