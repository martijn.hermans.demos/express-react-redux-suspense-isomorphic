import { Router } from 'express'
import React from 'react'
import ReactDOM from 'react-dom/server'
import { Provider as ReduxProvider } from 'react-redux'
import { Provider as SuspenseEffectProvider } from 'shared/hooks/use-suspense-effect'

import createDocument from './create-document'

import Routes from 'shared/config/routes'
import { getRoute } from 'shared/utils/get-route'
import createStore from 'shared/store'

const router = Router()
export default router

async function renderAsync (Component, {
  matches,
  store,
  timeout = 10000
}) {
  const render = (shouldRunEffects) =>
    ReactDOM.renderToString(
      <ReduxProvider store={store}>
        <SuspenseEffectProvider value={shouldRunEffects}>
          <Component {...matches} />
        </SuspenseEffectProvider>
      </ReduxProvider>
    )

  let start = Date.now()

  while (Date.now() < start + timeout) {
    try {
      render(true)
      break
    } catch (e) {
      if (!e || typeof e.then !== 'function') {
        break
      }
      await e
    }
  }
  return render(false)
}

router.get('*', async (req, res) => {
  const { matches, Component } = getRoute(req.originalUrl, Routes) || {}
  if (!Component) {
    res.status(404).send()
    return
  }
  const store = createStore()

  let html = await renderAsync(Component, { matches, store })

  res.send(
    createDocument({
      html,
      jsSrc: '/assets/js/main.js',
      preloadedState: store.getState()
    })
  )
})
