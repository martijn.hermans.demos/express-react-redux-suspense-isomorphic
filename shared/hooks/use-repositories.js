import { useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import useSuspenseEffect from 'shared/hooks/use-suspense-effect'
import * as Actions from 'shared/actions'

export default function useRepositories(query) {
  let repositories = useSelector(state => state.repositories)
  let dispatch = useDispatch()

  repositories = useMemo(() =>
      Array.isArray(repositories) ? repositories : [],
    [repositories]
  )

  useSuspenseEffect(() => {
    if (!repositories || repositories.length === 0) {
      return dispatch(Actions.fetchRepositories(query))
    }
  }, [repositories, dispatch, query])

  return repositories
}
