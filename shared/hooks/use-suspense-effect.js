import { createContext, useContext, useRef, useMemo } from 'react'

const SuspenseEffectContext = createContext(false)

let useSuspenseEffect

if (typeof window === 'undefined') {
  useSuspenseEffect = callback => {
    let shouldFire = useContext(SuspenseEffectContext)
    if (shouldFire) {
      let res = callback()
      if (res && typeof res.then === 'function')
        throw res
    }
  }
} else {
  useSuspenseEffect = function useAsyncEffect (callback, inputs) {
    const ref = useRef(false)
    let val = ref.current
    ref.current = useMemo(() => ! ref.current, inputs)
    if (ref.current !== val) {
      let res = callback()
      if (res && typeof res.then === 'function')
        throw res
    }
  }
}

export default useSuspenseEffect

export const Provider = SuspenseEffectContext.Provider
