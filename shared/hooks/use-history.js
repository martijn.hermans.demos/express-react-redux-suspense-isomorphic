import { createContext, useContext } from "react";

const HistoryContext = createContext();

export default function useHistory() {
  return useContext(HistoryContext);
}

export const Provider = HistoryContext.Provider;
