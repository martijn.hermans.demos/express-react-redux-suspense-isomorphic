import { useDispatch, useSelector } from 'react-redux'
import useSuspenseEffect from 'shared/hooks/use-suspense-effect'
import * as Actions from 'shared/actions'

export default function useRepository (owner, repo) {
  const repository = useSelector(state => state.repository[`${owner}/${repo}`])
  let dispatch = useDispatch()

  useSuspenseEffect(() => {
    if (!repository) {
      return dispatch(Actions.fetchRepository(owner, repo))
    }
  }, [dispatch, owner, repo])

  return repository
}
