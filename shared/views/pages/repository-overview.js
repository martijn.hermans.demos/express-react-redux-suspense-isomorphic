import React from 'react'
import RepositoryList from 'shared/components/repository-list'
import useRepositories from 'shared/hooks/use-repositories'

export default function RepositoryOverviewPage () {
  const query = 'react'

  let repositories = useRepositories(query)

  return (
    <div className="container">
      <h3>{query}</h3>
      <RepositoryList repositories={repositories} />
    </div>
  )
}
