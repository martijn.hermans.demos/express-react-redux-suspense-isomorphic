import React from 'react'
import Link from 'shared/components/link'
import useRepository from 'shared/hooks/use-repository'

export default function RepositoryDetailPage ({ owner, repo }) {
  const repository = useRepository(owner, repo)

  return (
    <div className="container">
      <h3>{repository.name}</h3>
      <p>{repository.description}</p>
      <Link href="/">Back to overview</Link>
    </div>
  )
}
